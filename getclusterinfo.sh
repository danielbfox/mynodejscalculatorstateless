#!/bin/bash

ZONE=us-east1-b

echo "Getting the cluster information for creating the cluster in GitLab"
echo "------------------------------------------------------------------"

CLUSTERNAME=$(kubectl config view -o json | jq -r ".clusters[0].name")
APIURL=$(kubectl config view -o json | jq -r ".clusters[0].cluster.server")
NODE=$(kubectl get nodes -o jsonpath='{range.items[0].metadata}{.name} {end}')
CA=$(gcloud compute ssh $NODE --zone=$ZONE --command='sudo cat /etc/srv/kubernetes/pki/ca-certificates.crt')
ADMINSECRET=$(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')
ADMINTOKEN=$(kubectl -n kube-system describe secret $ADMINSECRET | grep 'token: ' | awk '{print $2}')

echo "Fill in the following fields with the specified values :"
echo "Kubernetes cluster name = $CLUSTERNAME"
echo "API URL = $APIURL"
echo "CA Certificate = $CA"
echo "Token = $ADMINTOKEN"
echo "Project namespace = default"

exit 0
