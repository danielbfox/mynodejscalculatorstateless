# Setup the cluster

This has to be done once.    
Open google cloud console and type the commands :

```
cd
mkdir gitrepos
cd gitrepos  
git clone https://gitlab.com/danielbfox/mynodejscalculatorstateless.git
cd mynodejscalculatorstateless
./setup.sh 
```

# Configure gitlab

Still in the git `mynodejscalculatorstateless` repo folder, type the following command to get the GitLab cluster definition fields values:
```
./getclusterinfo.sh
```

Add the cluster to gitlab :
- Click `Operations` then `Kubernetes` then `Add Kubernetes cluster`
- Fill in the fields with the values shown by the script you just ran

# Deploy the v`1.0` of the application

Go to gitlab, then CI/CD, then `Run Pipeline`, then select branch `1.0` then `Create pipeline` and wait for the step `build` to be completed
Still from the same pipeline, click on the `play` button of the `install` step and wait for its completion

Then retrieve the IP address of the ingress by typing the following command :
```
kubectl get ingress mynodejscalculatorstateless-ingress -o json | jq -r ".status.loadBalancer.ingress[0].ip"
```
*Note that it can take a while because the IP address to be allocated and reachable*

Finally open your browser to http://`<TheIPYouJustRetrieved>` 

# Deploy an ugraded v`x.y` of the application

Go to gitlab and create a branch `x.y`
Do you change in the branch `x.y` *(Don't forget to change the version number displayed in the source file `mycalculatorstateless.js`)
Go to `CI/CD Pipelines` and wait for the `build` step to complete
Then click on the `play` button of the `upgrade` step and wait for its completion

Finally refresh your browser

# Delete the cluster

Still the git mynodejscalculatorstateless repo folder, type the command :

```
./cleanup.sh
```
